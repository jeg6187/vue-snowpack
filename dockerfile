FROM nginx:1-alpine
RUN apk add yarn
WORKDIR /app
COPY . .
RUN yarn
RUN yarn build
RUN cp nginx.conf /etc/nginx/nginx.conf